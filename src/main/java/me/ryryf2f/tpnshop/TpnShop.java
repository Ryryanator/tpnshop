package me.ryryf2f.tpnshop;

import me.ryryf2f.tpnlib.api.util.LogHandler;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public final class TpnShop extends JavaPlugin
{


    private static TpnShop instance;
    private static LogHandler logHandler;
    private static Economy econ;
    public static boolean debug = false;
    @Override
    public void onEnable()
    {
        instance = this;
        logHandler = new LogHandler(instance);
        setupVault();
    }

    @Override
    public void onDisable()
    {
        logHandler = null;
        instance = null;
    }

    private void setupVault()
    {
        if (!setupEconomy())
        {
            logHandler.severe("Disabled due to no Vault dependency found!");
            Bukkit.getPluginManager().disablePlugin(this);
            return;
        }
    }

    private boolean setupEconomy()
    {
        if (Bukkit.getPluginManager().getPlugin("Vault") == null)
        {
            return false;
        }

        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);

        if (rsp == null)
        {
            return false;
        }

        econ = rsp.getProvider();
        return econ != null;

    }

    public static TpnShop getInstance()
    {
        return this.instance;
    }

    public static LogHandler getLogHandler()
    {
        return logHandler;
    }

    public static Economy getEconomy()
    {
        return econ;
    }
}
