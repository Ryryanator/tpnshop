package me.ryryf2f.tpnshop.listeners.shop.sign;

import me.ryryf2f.tpnshop.events.PreShopCreationEvent;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;


import java.util.Optional;
import java.util.function.Function;

public class PreShopCreation implements Listener
{






    @EventHandler(priority = EventPriority.NORMAL)
    public void onPreShopCreation(PreShopCreationEvent event)
    {

        Optional<Sign> signOptional = Optional.of(event.getSign());



        isValidPrice(signOptional);
        isValidItem(signOptional);
        //this method does handle one permission
        isValidName(signOptional, event.getPlayer());
        //this handles the rest
        hasPermissions(event.getPlayer());


    }


    /**
     * Checks to make sure the formatting of the pricing exists and is correct
     */
    private boolean isValidPrice(Optional<Sign> sign)
    {


        return false;
    }

    /**
     * Checks if the name on the sign is valid
     * @return
     */
    private boolean isValidName(Optional<Sign> sign, Player player)
    {



        return false;
    }

    /**
     * Checks if the item is a valid and supported item
     * @return
     */
    private boolean isValidItem(Optional<Sign> sign)
    {


        return false;
    }
    /**
     * Figures out required permissions and returns if player can create shop
     * @return
     */
    private boolean hasPermissions(Player player)
    {

        //todo check if have permission to create sign, check if has permission to use the given username, etc.
        return false;
    }


}
