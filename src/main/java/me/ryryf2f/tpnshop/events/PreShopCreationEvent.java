package me.ryryf2f.tpnshop.events;

import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class PreShopCreationEvent extends Event implements Cancellable
{
    private static final HandlerList handlers = new HandlerList();

    private Player creator;

    private Outcome outcome = Outcome.SHOP_CREATED_SUCCESSFULLY;
    private Sign sign;
    private String[] signLines;

    public PreShopCreationEvent(Player creator, Sign sign, String[] signLines)
    {
        this.creator = creator;
        this.sign = sign;
        this.signLines = signLines.clone();
    }

    /**
     * returns whether or not the event is cancelled
     * @return
     */
    @Override
    public boolean isCancelled()
    {
        return outcome != Outcome.SHOP_CREATED_SUCCESSFULLY;
    }


    /**
     * Set the event cancelled. This sets a generic @link Outcome#OTHER
     * @param cancelled
     */
    @Override
    public void setCancelled(boolean cancelled)
    {
        outcome = Outcome.OTHER;
    }


    /**
     * Sets the event's outcome
     *
     * @param outcome Outcome
     */
    public void setOutcome(Outcome outcome)
    {
        this.outcome = outcome;
    }

    /**
     * Sets the shop's creator
     *
     * @param creator Shop's creator
     */
    public void setCreator(Player creator)
    {
        this.creator = creator;
    }

    /**
     * Sets the sign attached to the shop
     *
     * @param sign Shop sign
     */
    public void setSign(Sign sign)
    {
        this.sign = sign;
    }

    /**
     * Sets the text on the sign
     *
     * @param signLines Text to set
     */
    public void setSignLines(String[] signLines)
    {
        this.signLines = signLines;
    }

    /**
     * Sets one of the lines on the sign
     *
     * @param line Line number to set (0-3)
     * @param text Text to set
     */
    public void setSignLine(byte line, String text)
    {
        this.signLines[line] = text;
    }

    /**
     * Returns the shop's creator
     *
     * @return Shop's creator
     */
    public Player getPlayer()
    {
        return creator;
    }

    /**
     * Returns the shop's sign
     *
     * @return Shop's sign
     */
    public Sign getSign()
    {
        return sign;
    }

    /**
     * Returns the text on the sign
     *
     * @param line Line number (0-3)
     * @return Text on the sign
     */
    public String getSignLine(byte line)
    {
        return signLines[line];
    }

    /**
     * Returns the text on the sign
     *
     * @return Text on the sign
     */
    public String[] getSignLines()
    {
        return signLines;
    }

    public HandlerList getHandlers()
    {
        return handlers;
    }

    public static HandlerList getHandlerList()
    {
        return handlers;
    }










    public static enum Outcome
    {
        INVALID_ITEM,
        INVALID_PRICE,
        INVALID_QUANTITY,
        ITEM_AUTOFILL,
        UNKNOWN_PLAYER,
        SELL_PRICE_HIGHER_THAN_BUY_PRICE,
        SELL_PRICE_ABOVE_MAX,
        SELL_PRICE_BELOW_MIN,
        BUY_PRICE_ABOVE_MAX,
        BUY_PRICE_BELOW_MIN,
        NO_CHEST,
        NO_PERMISSION,
        NO_PERMISSION_FOR_TERRAIN,
        NO_PERMISSION_FOR_CHEST,
        NOT_ENOUGH_MONEY,

        /**
         * For plugin use
         */
        OTHER,

        SHOP_CREATED_SUCCESSFULLY
    }
}
