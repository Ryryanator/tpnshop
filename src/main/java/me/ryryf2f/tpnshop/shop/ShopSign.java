package me.ryryf2f.tpnshop.shop;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.OptionalInt;

public class ShopSign
{

    private static Sign sign;

    public ShopSign(Block block)
    {
        if (block.getType().equals(Material.SIGN) || block.getType().equals(Material.WALL_SIGN))
        {
            this.sign = (Sign) block;
        }
    }

    public ShopSign(Sign sign)
    {
        this.sign = sign;
    }

    public Optional<Sign> getSign()
    {
        return Optional.of(sign);
    }

    //todo find a faster regex to do this, seems clunky
    /**
     * Get the buying price
     * @return the double in an Optional
     */
    public OptionalDouble getBuy()
    {
        String[] lines = sign.getLine(1).split(":");
        OptionalDouble ret = OptionalDouble.empty();
        for (String i : lines)
        {
            if (i.contains("b") || i.contains("B"))
            {
                i.replaceAll("\\D+","");
                ret = OptionalDouble.of(Double.parseDouble(i));
            }
        }
        return ret;
    }

    /**
     * Get the selling price
     * @return the double in an Optional
     */
    public OptionalDouble getSell()
    {
        String[] lines = sign.getLine(1).split(":");
        OptionalDouble ret = OptionalDouble.empty();
        for (String i : lines)
        {
            if (i.contains("s") || i.contains("S"))
            {
                i.replaceAll("\\D+","");
                ret = OptionalDouble.of(Double.parseDouble(i));
            }
        }
        return ret;
    }

    /**
     * Get the owner
     * @return the owner string in an Optional
     */
    public Optional getOwner()
    {
        return Optional.of(sign.getLine(0));
    }

    public Optional getItem()
    {
        return Optional.of(sign.getLine(3));
    }

    public OptionalInt getAmount()
    {
        return OptionalInt.of(Integer.parseInt(sign.getLine(2)));
    }
}
